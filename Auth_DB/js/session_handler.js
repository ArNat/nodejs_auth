﻿const MongoStore = require('connect-mongo'); 


module.exports = {
    createStore: function () {
        return MongoStore.create ({ 
            mongoUrl: 'mongodb://localhost:27017/test' 
        });
          
    }
}
