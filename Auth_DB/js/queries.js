const MongoClient = require("mongodb").MongoClient;
const pass_handler = require('./password_handler');
//const db_handler = require('./db_handler');


const mongoClient = new MongoClient('mongodb://127.0.0.1:27017/');

module.exports = {
	//curr_user: '',
	// загрузить информацию о зарегистрированных пользователях 
	get_users: async function (req, res) {
		try {
			// Подключаемся к серверу
	
			await mongoClient.connect();
			
			const db = mongoClient.db("USERS");
			const collection = db.collection("users");
			const results = await collection.find().toArray();
			var docs = results.map((row) => {
				return '<h3> ' + row.username +' </h3>'
			});

			res.send(docs.join(''));
		}catch(err) {

			console.log("Возникла ошибка");
			
			console.log(err);
			
			} finally {
			
				// Закрываем подключение при завершении работы или при ошибке
				
				await mongoClient.close();
				
				}
			
			
			},
			
		add_user: async function(req, res){
			try {

				// Подключаемся к серверу
				
				await mongoClient.connect();
				
				const db = mongoClient.db("USERS");
				const collection = db.collection("users");
				 const user = {
					username: req.body.username,
					password_hash: pass_handler.encrypt_pass(req.body.password),
				  };
				 await collection.insertOne(user);
				 res.status(200).send('user created successfully!');
				 }catch(err) {
				
				console.log("Возникла ошибка");
				
				console.log(err);
				
				}finally {
			
					// Закрываем подключение при завершении работы или при ошибке
					
					await mongoClient.close();
					
					}
				
				},
		
	// проверить имя пользователя 
	check_user: async function (req, res) {
		//var self = this;
		try {
			// Подключаемся к серверу
	
			await mongoClient.connect();
			
			const db = mongoClient.db("USERS");
			const collection = db.collection("users");
			
		var login = req.body.username;
		var passw = pass_handler.encrypt_pass(req.body.password);
		console.log(req.body);
		
        const user = await collection.find({username: login, password_hash:passw}).toArray();
				
if(user[0] !== undefined) {
	req.session.username = user[0].username;
	res.status(200);
	res.send(user[0]);
}
else {
	req.session.username='';
	res.status(404);
	res.send('user not found!');}

}

catch(err){

console.log(err);

res.sendStatus(500);

}finally {
			
	// Закрываем подключение при завершении работы или при ошибке
	
	await mongoClient.close();
	
	}

}
}
					
	