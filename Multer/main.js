var express = require('express'); 

var port = 8080; 

var path = require('path'); 
var fs = require('fs'); 

const MongoClient = require("mongodb").MongoClient;

const mongoClient = new MongoClient('mongodb://127.0.0.1:27017/');


// multer - middleware для обработки данных формы в кодировке multipart/form-data 
var multer = require('multer'); 

// dest - директория для сохранения файлов, загружаемых на сервер 
var upload = multer({dest: __dirname + '/uploads'}); 

// указать, что будет загружен один файл с именем recfile. 
// имя файла может быть любым, но оно должно совпадать со значением атрибута name элемента формы input с типом file
// например, <input type="file" name="recfile" />
var type = upload.single('recfile'); 

var app = express(); 

app.get('/', function(req,res, next) {
	 res.sendFile(__dirname + '/index.html'); 

})

// указать файл при обработке POST запроса (второй аргумент метода app.post)
 app.post('/upload', type, function(req, res) { 
 
	 // загруженный файл доступен через свойство req.file
	 console.log(req.file); 
	 
	 // файл временного хранения данных
	 var tmp_path = req.file.path;
	 console.log(req.file.destination)
	 // место, куда файл будет загружен 
	 var target_path = path.join(req.file.destination, req.file.originalname); 
	
	  // загрузка файла 
	  var src = fs.createReadStream(tmp_path); 
	  var dest = fs.createWriteStream(target_path);
	  
	  src.pipe(dest); 
	  
	  // обработка результатов загрузки 
	  src.on('end', function() { 
	  
		// удалить файл временного хранения данных
		fs.unlink(tmp_path,(err) => {
			if (err) {
			  console.error(err);
			} else {
			  console.log('File deleted successfully!');
			}
		  });  
		save_path(target_path, res); 
		res.sendFile(__dirname + '/index.html'); 

	  });
	  src.on('error', function(err) { 
	  
	  	// удалить файл временного хранения данных
	    fs.unlink(tmp_path,(err) => {
			if (err) {
			  console.error(err);
			} else {
			  console.log('File deleted successfully!');
			}
		  });   
		res.send('error'); 
	  });

 }) 
 
 app.use(express.static(path.join(__dirname))); 
 

 
 async function save_path(target_path, res) {
	try {

		
		await mongoClient.connect();
		
		const db = mongoClient.db("FILES");
		
		 const collection = db.collection("files");
	 
	 var inserts = {
		 path: target_path
	 } 
	 console.log(target_path)
	 await collection.insertOne(inserts);
	 res.end(); 
	 }catch(err) {
	
	console.log("Возникла ошибка");
	console.log(err);
		}finally {

			// Закрываем подключение при завершении работы или при ошибке
			
			await mongoClient.close();
			
					
			}
   
 } 
 
 app.get('/get_uploads', async function(req, res) {
	// async function get_images(res) {
	 //async () =>{
	try {

		await mongoClient.connect();
		
		const db = mongoClient.db("FILES");
		
		 const collection = db.collection("files");
	     const results = await collection.find().toArray(); 
	 		 var images = results.map((row) => {
			 return `<img src="${row.path}" style="width:400px" />`
		 }); 
		 //var img_arr = JSON.stringify(images); 
		 //console.log(img_arr); 
		 res.send(images.join('')); 
	 }catch(err) {
	
		console.log("Возникла ошибка");
		console.log(err);
			}finally {
	
				// Закрываем подключение при завершении работы или при ошибке
				
				await mongoClient.close();
				
				
				}
			}
	   
 );
 
 app.listen(port, function() {
	    console.log('App listening on port ' + port);
 }); 